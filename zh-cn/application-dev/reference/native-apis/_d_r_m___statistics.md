# DRM_Statistics


## 概述

DRM度量统计信息。

**系统能力：** SystemCapability.Multimedia.Drm.Core

**起始版本：** 11

**相关模块：**[Drm](_drm.md)


## 汇总


### 成员变量

| 名称 | 描述 | 
| -------- | -------- |
| uint32_t [statisticsCount](_drm.md#statisticscount) | 度量信息数组长度 | 
| [DRM_CharBufferPair](_d_r_m___char_buffer_pair.md)[info](_drm.md#info) [0] | 度量信息数组 | 
