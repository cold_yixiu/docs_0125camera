# DRM_PsshInfo


## 概述

DRM Pssh信息。

**系统能力：** SystemCapability.Multimedia.Drm.Core

**起始版本：** 11

**相关模块：**[Drm](_drm.md)


## 汇总


### 成员变量

| 名称 | 描述 | 
| -------- | -------- |
| char [uuid](_drm.md#uuid) [[DRM_UUID_LEN](_drm.md#drm_uuid_len)] | DRM插件类型名。 | 
| uint32_t [dataLen](_drm.md#datalen) | PSSH数据长度 | 
| unsigned char \* [data](_drm.md#data-33) | PSSH数据 | 
