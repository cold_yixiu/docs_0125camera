# DRM_CharBufferPair


## 概述

字符数组类型的名值对。

**系统能力：** SystemCapability.Multimedia.Drm.Core

**起始版本：** 11

**相关模块：**[Drm](_drm.md)


## 汇总


### 成员变量

| 名称 | 描述 | 
| -------- | -------- |
| [DRM_CharBuffer](_d_r_m___char_buffer.md)[name](_drm.md#name) | 名字 | 
| [DRM_CharBuffer](_d_r_m___char_buffer.md)[value](_drm.md#value-12) | 值 | 
