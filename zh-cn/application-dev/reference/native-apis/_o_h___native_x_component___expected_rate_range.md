# OH_NativeXComponent_ExpectedRateRange


## 概述

定义期望帧率范围。

**起始版本：** 11

**相关模块：**[Native XComponent](_o_h___native_x_component.md)


## 汇总


### 成员变量

| 名称 | 描述 |
| -------- | -------- |
| [min](_o_h___native_x_component.md#min) | 期望帧率范围最小值。 |
| [max](_o_h___native_x_component.md#max) | 期望帧率范围最大值。 |
| [expected](_o_h___native_x_component.md#expected) | 期望帧率。 |
