# DRM_Uint8Buffer


## 概述

DRM uint_8 数组类型。

**系统能力：** SystemCapability.Multimedia.Drm.Core

**起始版本：** 11

**相关模块：**[Drm](_drm.md)


## 汇总


### 成员变量

| 名称 | 描述 | 
| -------- | -------- |
| unsigned char \* [buffer](_drm.md#buffer-12) | 数组首地址 | 
| uint32_t [bufferLen](_drm.md#bufferlen-12) | 数组长度 | 
