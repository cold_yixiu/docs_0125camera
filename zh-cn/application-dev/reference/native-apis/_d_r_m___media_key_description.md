# DRM_MediaKeyDescription


## 概述

在线许可证描述信息。

**系统能力：** SystemCapability.Multimedia.Drm.Core

**起始版本：** 11

**相关模块：**[Drm](_drm.md)


## 汇总


### 成员变量

| 名称 | 描述 | 
| -------- | -------- |
| uint32_t [mediaKeyCount](_drm.md#mediakeycount) | 许可证信息数组长度 | 
| [DRM_CharBufferPair](_d_r_m___char_buffer_pair.md)[description](_drm.md#description) [0] | 许可证信息数组 | 
