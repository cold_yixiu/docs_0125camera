# DRM_MediaKeyRequest


## 概述

许可证请求类型。

**系统能力：** SystemCapability.Multimedia.Drm.Core

**起始版本：** 11

**相关模块：**[Drm](_drm.md)


## 汇总


### 成员变量

| 名称 | 描述 | 
| -------- | -------- |
| [DRM_MediaKeyRequestType](_drm.md#drm_mediakeyrequesttype)[type](_drm.md#type-22) | 许可证请求类型 | 
| [DRM_Uint8Buffer](_d_r_m___uint8_buffer.md)[data](_drm.md#data-23) | 许可证请求数据 | 
| [DRM_CharBuffer](_d_r_m___char_buffer.md)[defaultUrl](_drm.md#defaulturl) | 许可证服务器URL | 
