# ImagePacker_Opts_


## 概述

定义图像编码选项信息。

**系统能力：** SystemCapability.Multimedia.Image

**起始版本：** 11

**相关模块：** [Image](image.md)


## 汇总


### 成员变量

| 名称 | 描述 | 
| -------- | -------- |
| [format](#format) | 编码格式 | 
| [quality](#quality) | 编码质量 | 


## 结构体成员变量说明


### format

```
const char* ImagePacker_Opts_::format
```

**描述**

编码格式


### quality

```
int ImagePacker_Opts_::quality
```

**描述**

编码质量
