# DRM_CharBuffer


## 概述

DRM 字符数组类型。

**系统能力：** SystemCapability.Multimedia.Drm.Core

**起始版本：** 11

**相关模块：**[Drm](_drm.md)


## 汇总


### 成员变量

| 名称 | 描述 | 
| -------- | -------- |
| char \* [buffer](_drm.md#buffer-22) | 数组首地址 | 
| uint32_t [bufferLen](_drm.md#bufferlen-22) | 数组长度 | 
