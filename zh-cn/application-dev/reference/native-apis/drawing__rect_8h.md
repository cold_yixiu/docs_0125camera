# drawing_rect.h


## 概述

文件中定义了与矩形相关的功能函数。

**引用文件：**&lt;native_drawing/drawing_rect.h&gt;

**库：** libnative_drawing.so

**起始版本：** 11

**相关模块：**[Drawing](_drawing.md)


## 汇总


### 函数

| 名称 | 描述 |
| -------- | -------- |
| [OH_Drawing_Rect](_drawing.md#oh_drawing_rect) \* [OH_Drawing_RectCreate](_drawing.md#oh_drawing_rectcreate) (float left, float top, float right, float bottom) | 用于创建一个矩形对象。 |
| void [OH_Drawing_RectDestroy](_drawing.md#oh_drawing_rectdestroy) ([OH_Drawing_Rect](_drawing.md#oh_drawing_rect) \*) | 用于销毁矩形对象并回收该对象占有的内存。 |
