# DRM_Uint8CharBufferPair


## 概述

整形数组类型的名值对。

**系统能力：** SystemCapability.Multimedia.Drm.Core

**起始版本：** 11

**相关模块：**[Drm](_drm.md)


## 汇总


### 成员变量

| 名称 | 描述 | 
| -------- | -------- |
| [DRM_Uint8Buffer](_d_r_m___uint8_buffer.md)[key](_drm.md#key) | 名字 | 
| [DRM_CharBuffer](_d_r_m___char_buffer.md)[value](_drm.md#value-22) | 值 | 
