# DRM_KeysInfo


## 概述

密钥信息。

**系统能力：** SystemCapability.Multimedia.Drm.Core

**起始版本：** 11

**相关模块：**[Drm](_drm.md)


## 汇总


### 成员变量

| 名称 | 描述 | 
| -------- | -------- |
| uint32_t [keysCount](_drm.md#keyscount) | 密钥信息数组长度 | 
| [DRM_Uint8CharBufferPair](_d_r_m___uint8_char_buffer_pair.md)[keysInfo](_drm.md#keysinfo) [0] | 密钥信息数组 | 
