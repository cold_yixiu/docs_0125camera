# DRM_MediakeyIdArray


## 概述

离线许可证Id数组。

**系统能力：** SystemCapability.Multimedia.Drm.Core

**起始版本：** 11

**相关模块：**[Drm](_drm.md)


## 汇总


### 成员变量

| 名称 | 描述 | 
| -------- | -------- |
| uint32_t [mediaKeyIdCount](_drm.md#mediakeyidcount) | 许可证Id数组长度 | 
| [DRM_Uint8Buffer](_d_r_m___uint8_buffer.md)[mediaKeyIds](_drm.md#mediakeyids) [0] | 许可证Id数组 | 
