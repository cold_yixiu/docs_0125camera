# native_drm_err.h


## 概述

定义DRM错误码

**库：** libnative_drm.z.so

**系统能力：** SystemCapability.Multimedia.Drm.Core

**起始版本：** 11

**相关模块：**[Drm](_drm.md)


## 汇总


### 类型定义

| 名称 | 描述 | 
| -------- | -------- |
| typedef enum [Drm_ErrCode](_drm.md#drm_errcode)[Drm_ErrCode](_drm.md#drm_errcode) | DRM错误码。 | 


### 枚举

| 名称 | 描述 | 
| -------- | -------- |
| [Drm_ErrCode](_drm.md#drm_errcode) {<br/>DRM_ERR_OK = 0,<br/>DRM_ERR_NO_MEMORY,<br/>DRM_ERR_OPERATION_NOT_PERMITTED,<br/>DRM_ERR_INVALID_VAL,<br/>DRM_ERR_IO,<br/>DRM_ERR_TIMEOUT,<br/>DRM_ERR_UNKNOWN,<br/>DRM_ERR_SERVICE_DIED,<br/>DRM_ERR_INVALID_STATE,<br/>DRM_ERR_UNSUPPORTED,<br/>DRM_ERR_MAX_SYSTEM_NUM_REACHED,<br/>DRM_ERR_MAX_SESSION_NUM_REACHED,<br/>DRM_ERR_EXTEND_START = 100<br/>} | DRM错误码。 | 
