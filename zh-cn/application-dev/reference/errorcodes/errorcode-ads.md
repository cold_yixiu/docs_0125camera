# 广告服务框架错误码


> **说明：**
> 以下仅介绍本模块特有错误码，通用错误码请参考[通用错误码说明文档](errorcode-universal.md)。


## 21800001 系统内部错误

**错误信息**

System internal error.

**错误描述**

系统内部错误。

**可能原因**

连接服务失败。

**处理步骤**

检查系统服务是否运行正常。


## 21800003 广告请求加载失败

**错误信息**

Failed to load the ad request.

**错误描述**

广告请求加载失败。

**可能原因**

网络连接异常或广告请求参数错误。

**处理步骤**

请检查网络状态或广告请求参数是否符合要求。


## 21800004 广告展示失败

**错误信息**

Failed to display the ad.

**错误描述**

广告展示失败。

**可能原因**

网络连接异常。

**处理步骤**

请检查网络状态。
