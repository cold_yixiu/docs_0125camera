# 系统公共事件定义

本文档提供系统所定义的公共事件类型的索引。
公共事件类型定义在[ohos.commonEventManager模块的Support枚举](./js-apis-commonEventManager.md#support)中。

**系统能力：** SystemCapability.Notification.CommonEvent

* COMMON_EVENT_SPLIT_SCREEN
表示分屏的公共事件的动作。

* [COMMON_EVENT_SCREEN_LOCKED <sup>10+</sup> ](./common_event/commonEvent-screenlock.md#common_event_screen_locked)
表示屏幕锁定的公共事件。

* [COMMON_EVENT_SCREEN_UNLOCKED<sup>10+</sup> ](./common_event/commonEvent-screenlock.md#common_event_screen_unlocked)
表示屏幕解锁的公共事件。

* [COMMON_EVENT_CONNECTIVITY_CHANGE<sup>10+</sup> ](./common_event/commonEvent-netmanager.md#common_event_connectivity_change10)
提示网络连接状态变化。