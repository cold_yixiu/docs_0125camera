# @ohos.multimedia.avCastPickerParam (投播组件参数)

avCastPickerParam提供了[@ohos.multimedia.avCastPicker](../arkui-ts/ohos-multimedia-avcastpicker.md)窗口状态枚举值。

> **说明：**
>
> 本模块首批接口从API version 11开始支持。后续版本的新增接口，采用上角标单独标记接口的起始版本。

## AVCastPickerState

投播状态参数选项。

**系统能力：** SystemCapability.Multimedia.AVSession.AVCast

| 类型   | 说明    |
|------|-------|
| STATE_APPEARING    | 组件显示。 |
| STATE_DISAPPEARING    | 组件消失。 |