# Input Kit（多模输入服务）

- [Input Kit简介](../device/input-overview.md)
- 多模开发指导
  - [输入设备开发指导](../device/inputdevice-guidelines.md)
  - [鼠标光标开发指导](../device/pointerstyle-guidelines.md)
  - [输入监听开发指导](../device/inputmonitor-guidelines.md)
  - [事件注入开发指导](../device/inputeventclient-guidelines.md)
  - [组合按键开发指导](../device/inputconsumer-guidelines.md)
  - [快捷键开发指导](../device/shortkey-guidelines.md)