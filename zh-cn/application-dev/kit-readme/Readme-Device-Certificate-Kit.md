# Device Certificate Kit（设备证书服务）

- [Device Certificate Kit简介](../security/DeviceCertificateKit/device-certificate-kit-intro.md)
- 证书算法库框架
  - [证书算法库框架概述](../security/DeviceCertificateKit/certificate-framework-overview.md)
  - [证书对象的创建、解析和校验](../security/DeviceCertificateKit/create-parse-verify-cert-object.md)
  - [证书扩展信息对象的创建、解析和校验](../security/DeviceCertificateKit/create-parse-verify-certextension-object.md)
  - [证书吊销列表对象的创建、解析和校验](../security/DeviceCertificateKit/create-parse-verify-crl-object.md)
  - [证书链校验器对象的创建和校验](../security/DeviceCertificateKit/create-verify-cerchainvalidator-object.md)
  - [证书集合及证书吊销列表集合对象的创建和获取](../security/DeviceCertificateKit/create-get-cert-crl-object.md)
  - [证书链对象的创建和校验](../security/DeviceCertificateKit/create-verify-certchain-object.md)
- 证书管理
  - [证书管理概述](../security/DeviceCertificateKit/certManager-overview.md)
  - [证书管理开发指导](../security/DeviceCertificateKit/certManager-guidelines.md)
