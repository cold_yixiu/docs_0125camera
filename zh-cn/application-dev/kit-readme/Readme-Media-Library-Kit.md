# Media Library Kit（媒体文件管理服务）

- [Media Library Kit简介](../file-management/photoAccessHelper-overview.md)
- [开发准备](../file-management/photoAccessHelper-preparation.md)
- [媒体资源使用指导](../file-management/photoAccessHelper-resource-guidelines.md)
- [用户相册资源使用指导](../file-management/photoAccessHelper-userAlbum-guidelines.md)
- [系统相册资源使用指导](../file-management/photoAccessHelper-systemAlbum-guidelines.md)
- [媒体资源变更通知相关指导](../file-management/photoAccessHelper-notify-guidelines.md)