# Network Kit（网络服务）
- [Network Kit简介](../connectivity/net-mgmt-overview.md)

- Network Kit数据传输能力
    - [HTTP数据请求](../connectivity/http-request.md)
    - [WebSocket连接](../connectivity/websocket-connection.md)
    - [Socket连接](../connectivity/socket-connection.md)
    - [MDNS](../connectivity/net-mdns.md)

- Network Kit网络管理能力
    - [网络连接管理](../connectivity/net-connection-manager.md)
    - [网络共享](../connectivity/net-sharing.md)
    - [以太网连接管理](../connectivity/net-ethernet.md)