## IME Kit（输入法开发服务）

- [IME Kit简介](../inputmethod/ime-kit-intro.md)
- [实现一个输入法应用](../inputmethod/inputmethod_application_guide.md)
- [实现一个自绘编辑框](../inputmethod/custom_input_box_guide.md)
- [切换输入法应用](../inputmethod/switch_inputmehod_guide.md)
- [输入法子类型开发指南](../inputmethod/input_method_subtype_guide.md)