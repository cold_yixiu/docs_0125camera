# DRM Kit（数字版权保护服务）

- [DRM Kit 简介](../media/drm-overview.md)
- 数字版权保护开发指导(ArkTS)
  - [插件管理(ArkTS)](../media/drm-plugin-management.md)
  - [系统管理(ArkTS)](../media/drm-mediakeysystem-management.md)
  - [会话管理(ArkTS)](../media/drm-mediakeysession-management.md)
- 数字版权保护开发指导(C/C++)
  - [系统管理(C/C++)](../media/native-drm-mediakeysystem-management.md)
  - [会话管理(C/C++)](../media/native-drm-mediakeysession-management.md)