# User Authentication Kit（用户认证服务）

- [User Authentication Kit简介](../security/UserAuthenticationKit/user-authentication-overview.md)
- 用户身份认证开发指导
  - [开发准备](../security/UserAuthenticationKit/prerequisites.md)
  - [查询支持的认证能力](../security/UserAuthenticationKit/obtain-supported-authentication-capabilities.md)
  - [发起认证](../security/UserAuthenticationKit/start-authentication.md)
  - [认证过程中取消认证](../security/UserAuthenticationKit/cancel-authentication.md)
  - [切换自定义认证](../security/UserAuthenticationKit/apply-custom-authentication.md)
