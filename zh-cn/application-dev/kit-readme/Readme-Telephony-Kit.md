# Telephony Kit（蜂窝通信服务）
- [Telephony Kit简介](../telephony/telephony-overview.md)

- [拨打电话](../telephony/telephony-call.md)
- [短信服务](../telephony/telephony-sms.md)
