# Notification Kit（用户通知服务）

- [Notification Kit简介](../../application-dev/notification/notification-overview.md)
- [请求通知授权](../../application-dev/notification/notification-enable.md)
- [管理通知角标](../../application-dev/notification/notification-badge.md)
- [管理通知渠道](../../application-dev/notification/notification-slot.md)
- 发布通知
  - [发布文本类型通知](../../application-dev/notification/text-notification.md)
  - [发布进度条类型通知](../../application-dev/notification/progress-bar-notification.md)
  - [为通知添加行为意图](../../application-dev/notification/notification-with-wantagent.md)