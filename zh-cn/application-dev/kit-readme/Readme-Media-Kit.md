# Media Kit（媒体服务）

- [Media Kit简介](../media/media-kit-intro.md)
- 音频播放和录制
  - [使用AVPlayer开发音频播放功能(ArkTS)](../media/using-avplayer-for-playback.md)
  - [使用AVPlayer开发音频播放功能(C/C++)](../media/using-ndk-avplayer-for-playerback.md)
  - [使用SoundPool开发音频播放功能(ArkTS)](../media/using-soundpool-for-playback.md)
  - [使用AVRecorder开发音频录制功能(ArkTS)](../media/using-avrecorder-for-recording.md)
  - [获取音视频元数据(ArkTS)](../media/avmetadataextractor.md)
- 视频播放和录制
  - [视频播放(ArkTS)](../media/video-playback.md)
  - [视频录制(ArkTS)](../media/video-recording.md)
  - [屏幕录制(仅对系统应用开放)(C/C++)](../media/avscreen-capture.md)
  - [获取视频缩略图(ArkTS)](../media/avimagegenerator.md)
