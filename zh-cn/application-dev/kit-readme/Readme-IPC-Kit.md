# IPC Kit（进程间通信服务）

- [IPC Kit简介](../connectivity/ipc-rpc-overview.md)
- [IPC与RPC通信开发指导](../connectivity/ipc-rpc-development-guideline.md)
- [远端状态订阅开发实例](../connectivity/subscribe-remote-state.md)
