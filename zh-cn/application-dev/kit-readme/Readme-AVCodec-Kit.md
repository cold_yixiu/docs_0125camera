# AVCodec Kit（音视频编解码服务）

- [AVCodec Kit简介](../media/avcodec-kit-intro.md)
- 音视频编解码
  - [获取支持的编解码能力(C/C++)](../media/obtain-supported-codecs.md)
  - [音频编码(C/C++)](../media/audio-encoding.md)
  - [音频解码(C/C++)](../media/audio-decoding.md)
  - [视频编码(C/C++)](../media/video-encoding.md)
  - [视频解码(C/C++)](../media/video-decoding.md)
- 文件解析封装
  - [音视频封装(C/C++)](../media/audio-video-muxer.md)
  - [音视频解封装(C/C++)](../media/audio-video-demuxer.md)
