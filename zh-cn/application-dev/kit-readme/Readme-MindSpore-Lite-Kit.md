# MindSpore Lite Kit (昇思推理框架服务)

- [MindSpore Lite Kit简介](../ai/MindSpore-Lite-Kit-Introduction.md)
- [使用MindSpore Lite JS API开发AI应用](../ai/mindspore-guidelines-based-js.md)
- [使用MindSpore Lite Native API开发AI应用](../ai/mindspore-guidelines-based-native.md)
