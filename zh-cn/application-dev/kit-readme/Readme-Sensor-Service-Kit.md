# Sensor Service Kit（传感器服务）

- [Sensor Service Kit简介](../device/sensorservice-kit-intro.md)

- 传感器

  - [开发概述](../device/sensor-overview.md)
  - [开发指南](../device/sensor-guidelines.md)
	
- 振动

  - [开发概述](../device/vibrator-overview.md)
  - [开发指南](../device/vibrator-guidelines.md)