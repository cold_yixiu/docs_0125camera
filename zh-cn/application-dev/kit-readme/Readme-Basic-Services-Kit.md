## Basic Services Kit（基础服务）

- [Basic Services Kit简介](../application-models/basic-services-kit-overview.md)
- Basic Services Kit开发指南
  - 应用事件
    - 进程间通信
      - [公共事件简介](../application-models/common-event-overview.md)
      - 公共事件订阅
        - [公共事件订阅概述](../application-models/common-event-subscription-overview.md)
        - [动态订阅公共事件](../application-models/common-event-subscription.md)
        - [取消动态订阅公共事件](../application-models/common-event-unsubscription.md)
      - [公共事件发布](../application-models/common-event-publish.md)
    - 线程间通信
      -  [使用Emitter进行线程间通信](../application-models/itc-with-emitter.md)
  - 应用帐号
    - [管理应用帐号](../account/manage-application-account.md)
  - USB服务
    - [USB服务开发概述](../device/usb-overview.md)
    - [USB服务开发指导](../device/usb-guidelines.md)