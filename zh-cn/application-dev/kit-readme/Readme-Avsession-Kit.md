# AVSession Kit（音视频播控服务）

- [AVSession Kit简介](../media/avsession-overview.md)
- 本地媒体会话
  - [本地媒体会话概述](../media/local-avsession-overview.md)
  - [媒体会话提供方(ArkTS)](../media/using-avsession-developer.md)
  - [应用接入AVSession场景介绍](../media/avsession-access-scene.md)
  - [媒体会话控制方(ArkTS)](../media/using-avsession-controller.md)
- 分布式媒体会话
  - [分布式媒体会话概述](../media/distributed-avsession-overview.md)
  - [使用分布式媒体会话(ArkTS)](../media/using-distributed-avsession.md)