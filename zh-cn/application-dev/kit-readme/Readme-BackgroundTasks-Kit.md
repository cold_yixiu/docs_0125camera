## Background Tasks Kit（后台任务开发服务）

- [Background Tasks Kit简介](../task-management/background-task-overview.md)
- [短时任务](../task-management/transient-task.md)
- [长时任务](../task-management/continuous-task.md)
- [延迟任务](../task-management/work-scheduler.md)
- [代理提醒](../task-management/agent-powered-reminder.md)
- [能效资源申请（仅对系统特权应用开放）](../task-management/efficiency-resource-request.md)