# ArkWeb（方舟Web）

- [ArkWeb简介](../web/web-component-overview.md)
- [使用Web组件加载页面](../web/web-page-loading-with-web-components.md)
- 设置基本属性和事件
  - [设置深色模式](../web/web-set-dark-mode.md)
  - [上传文件](../web/web-file-upload.md)
  - [在新窗口中打开页面](../web/web-open-in-new-window.md)
  - [管理位置权限](../web/web-geolocation-permission.md)
- 在应用中使用前端页面JavaScript
  - [应用侧调用前端页面函数](../web/web-in-app-frontend-page-function-invoking.md)
  - [前端页面调用应用侧函数](../web/web-in-page-app-function-invoking.md)
  - [建立应用侧与前端页面数据通道](../web/web-app-page-data-channel.md)
- [管理页面跳转及浏览记录导航](../web/web-redirection-and-browsing-history-mgmt.md)
- [管理Cookie及数据存储](../web/web-cookie-and-data-storage-mgmt.md)
- [自定义页面请求响应](../web/web-resource-interception-request-mgmt.md)
- [使用Devtools工具调试前端页面](../web/web-debugging-with-devtools.md)