# Development Overview


The OpenHarmony Native Development Kit (NDK) provides a variety of open capability libraries, such as graphics, memory management, and device management, for you to implement code logic. It also comes with industry standard libraries, including [libc](../reference/native-lib/third_party_libc/musl.md), [libc++](../reference/native-lib/third_party_libc/cpp.md), and [Node-API](napi-introduction.md).
