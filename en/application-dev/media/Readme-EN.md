# Media

- [Media Application Development Overview](media-application-overview.md)
- Audio Kit
  - [Introduction to Audio Kit](audio-kit-intro.md)
  - Audio Playback
    - [Audio Playback Overview](audio-playback-overview.md)
    - [Using AudioRenderer for Audio Playback (ArkTS)](using-audiorenderer-for-playback.md)
    - [Using TonePlayer for Audio Playback (for System Applications Only) (ArkTS)](using-toneplayer-for-playback.md)
    - [Using OHAudio for Audio Playback (C/C++)](using-ohaudio-for-playback.md)
    - [Using OpenSL ES for Audio Playback (C/C++)](using-opensl-es-for-playback.md)
    - [Audio Playback Concurrency Policy (ArkTS)](audio-playback-concurrency.md)
    - [Volume Management (ArkTS)](volume-management.md)
    - [Audio Effect Management (ArkTS)](audio-effect-management.md)
    - [Audio Playback Stream Management (ArkTS)](audio-playback-stream-management.md)
    - [Audio Output Device Management (ArkTS)](audio-output-device-management.md)
    - [Distributed Audio Playback (for System Applications Only) (ArkTS)](distributed-audio-playback.md)
  - Audio Recording
    - [Audio Recording Overview](audio-recording-overview.md)
    - [Using AudioCapturer for Audio Recording (ArkTS)](using-audiocapturer-for-recording.md)
    - [Using OpenSL ES for Audio Recording (C/C++)](using-opensl-es-for-recording.md)
    - [Using OHAudio for Audio Recording (C/C++)](using-ohaudio-for-recording.md)
    - [Microphone Management (ArkTS)](mic-management.md)
    - [Audio Recording Stream Management (ArkTS)](audio-recording-stream-management.md)
    - [Audio Input Device Management (ArkTS)](audio-input-device-management.md)
  - Audio Call
    - [Audio Call Overview](audio-call-overview.md)
    - [Developing Audio Call (ArkTS)](audio-call-development.md)
- Media Kit
  - [Introduction to Media Kit](media-kit-intro.md)
  - Audio Playback and Recording
    - [Using AVPlayer for Audio Playback (ArkTS)](using-avplayer-for-playback.md)
    - [Using AVPlayer for Audio Playback (C/C++)](using-ndk-avplayer-for-playerback.md)
    - [Using SoundPool for Audio Playback (ArkTS)](using-soundpool-for-playback.md)
    - [Using AVRecorder for Audio Recording (ArkTS)](using-avrecorder-for-recording.md)
    - [Obtaining Audio/Video Metadata (ArkTS)](avmetadataextractor.md)
  - Video Playback and Recording
    - [Video Playback (ArkTS)](video-playback.md)
    - [Video Recording (ArkTS)](video-recording.md)
    - [Screen Capture (for System Applications Only) (C/C++)](avscreen-capture.md)
    - [Obtaining Video Thumbnails (ArkTS)](avimagegenerator.md)
- AVSession Kit
  - [Introduction to AVSession Kit](avsession-overview.md)
  - Local AVSession
    - [Local AVSession Overview](local-avsession-overview.md)
    - [AVSession Provider (ArkTS)](using-avsession-developer.md)
    - [Accessing AVSession](avsession-access-scene.md)
    - [AVSession Controller (ArkTS)](using-avsession-controller.md)
  - Distributed AVSession
    - [Distributed AVSession Overview](distributed-avsession-overview.md)
    - [Using Distributed AVSession (ArkTS)](using-distributed-avsession.md)
- AVCodec Kit
  - [Introduction to AVCodec Kit](avcodec-kit-intro.md)
  - Audio and Video Codecs
    - [Obtaining Supported Codecs (C/C++)](obtain-supported-codecs.md)
    - [Audio Encoding (C/C++)](audio-encoding.md)
    - [Audio Decoding (C/C++)](audio-decoding.md)
    - [Video Encoding (C/C++)](video-encoding.md)
    - [Video Decoding (C/C++)](video-decoding.md)
  - File Muxing and Demuxing
    - [Audio and Video Muxing (C/C++)](audio-video-muxer.md)
    - [Audio and Video Demuxing (C/C++)](audio-video-demuxer.md)
- Camera Kit
  - [Camera Development Preparations](camera-preparation.md)
  - Camera Development (ArkTS)
    - [Device Input Management (ArkTS)](camera-device-input.md)
    - [Camera Session Management (ArkTS)](camera-session-management.md)
    - [Camera Preview (ArkTS)](camera-preview.md)
    - [Camera Photographing (ArkTS)](camera-shooting.md)
    - [Camera Recording (ArkTS)](camera-recording.md)
    - [Camera Metadata (ArkTS)](camera-metadata.md)
  - Camera Best Practices (ArkTS)
    - [Sample Implementation of Camera Photographing (ArkTS)](camera-shooting-case.md)
    - [Sample Implementation of Camera Recording (ArkTS)](camera-recording-case.md)
    - [Dual-Channel Preview (ArkTS)](camera-dual-channel-preview.md)
    - [Using Performance Improvement Features (for System Applications Only) (ArkTS)](camera-performance-improvement.md)
  - Camera Development (C/C++)
    - [Device Input Management (C/C++)](native-camera-device-input.md)
    - [Camera Session Management (C/C++)](native-camera-session-management.md)
    - [Camera Preview (C/C++)](native-camera-preview.md)
    - [Secondary Processing of Preview Streams (C/C++)](native-camera-preview-imageReceiver.md)
    - [Camera Photographing (C/C++)](native-camera-shooting.md)
    - [Camera Recording (C/C++)](native-camera-recording.md)
    - [Secondary Processing of Video Streams (C/C++)](native-camera-recording-imageReceiver.md)
    - [Camera Metadata (C/C++)](native-camera-metadata.md)
  - Camera Best Practices (C/C++)
    - [Sample Implementation of Camera Photographing (C/C++)](native-camera-shooting-case.md)
    - [Sample Implementation of Camera Recording (C/C++)](native-camera-recording-case.md)
    - [Sample Implementation of Secondary Processing of Recording Streams (C/C++)](native-camera-recording-case-imageReceiver.md)
- Image Kit
  - [Introduction to Image Kit](image-overview.md)
  - [Image Decoding (ArkTS)](image-decoding.md)
  - [Image Decoding (C/C++)](image-decoding-native.md)
  - Image Processing
    - [Image Transformation (ArkTS)](image-transformation.md)
    - [Image Transformation (C/C++)](image-transformation-native.md)
    - [PixelMap Data Processing (C/C++)](image-pixelmap-operation-native.md)
    - [PixelMap Operation (ArkTS)](image-pixelmap-operation.md)
  - [Image Encoding (ArkTS)](image-encoding.md)
  - [Image Encoding (C/C++)](image-encoding-native.md)
  - [Image Tool (ArkTS)](image-tool.md)
