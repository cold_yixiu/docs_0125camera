# AVCodec Kit

- [Introduction to AVCodec Kit](../media/avcodec-kit-intro.md)
- Audio and Video Codecs
  - [Obtaining Supported Codecs (C/C++)](../media/obtain-supported-codecs.md)
  - [Audio Encoding (C/C++)](../media/audio-encoding.md)
  - [Audio Decoding (C/C++)](../media/audio-decoding.md)
  - [Video Encoding (C/C++)](../media/video-encoding.md)
  - [Video Decoding (C/C++)](../media/video-decoding.md)
- File Muxing and Demuxing
  - [Audio and Video Muxing (C/C++)](../media/audio-video-muxer.md)
  - [Audio and Video Demuxing (C/C++)](../media/audio-video-demuxer.md)
