# Notification Kit

- [Introduction to Notification Kit](../../application-dev/notification/notification-overview.md)
- [Requesting Notification Authorization](../../application-dev/notification/notification-enable.md)
- [Managing the Notification Badge](../../application-dev/notification/notification-badge.md)
- [Managing Notification Slots](../../application-dev/notification/notification-slot.md)
- Publishing a Notification
  - [Publishing a Text Notification](../../application-dev/notification/text-notification.md)
  - [Publishing a Progress Notification](../../application-dev/notification/progress-bar-notification.md)
  - [Adding a WantAgent Object to a Notification](../../application-dev/notification/notification-with-wantagent.md)
