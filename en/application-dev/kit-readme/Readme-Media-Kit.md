# Media Kit

- [Introduction to Media Kit](../media/media-kit-intro.md)
- Audio Playback and Recording
  - [Using AVPlayer for Audio Playback (ArkTS)](../media/using-avplayer-for-playback.md)
  - [Using AVPlayer for Audio Playback (C/C++)](../media/using-ndk-avplayer-for-playerback.md)
  - [Using SoundPool for Audio Playback (ArkTS)](../media/using-soundpool-for-playback.md)
  - [Using AVRecorder for Audio Recording (ArkTS)](../media/using-avrecorder-for-recording.md)
  - [Obtaining Audio/Video Metadata (ArkTS)](../media/avmetadataextractor.md)
- Video Playback and Recording
  - [Video Playback (ArkTS)](../media/video-playback.md)
  - [Video Recording (ArkTS)](../media/video-recording.md)
  - [Screen Capture (for System Applications Only) (C/C++)](../media/avscreen-capture.md)
  - [Obtaining Video Thumbnails (ArkTS)](../media/avimagegenerator.md)
