# Sensor Service Kit

- [Introduction to Sensor Service Kit](../device/sensorservice-kit-intro.md)

- Sensor

  - [Sensor Overview](../device/sensor-overview.md)
  - [Sensor Development](../device/sensor-guidelines.md)
	
- Vibrator

  - [Vibrator Overview](../device/vibrator-overview.md)
  - [Vibrator Development](../device/vibrator-guidelines.md)
