# Data Loss Prevention Kit (DLP Kit)

- [Introduction to DLP Kit](../security/DataLossPreventionKit/dlp-overview.md)
- [DLP Kit Development](../security/DataLossPreventionKit/dlp-guidelines.md)
