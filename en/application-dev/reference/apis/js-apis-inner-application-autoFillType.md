# AutoFillType

AutoFillType enumerates the types of elements to be automatically filled in.

> **NOTE**
> 
> The initial APIs of this module are supported since API version 11. Newly added APIs will be marked with a superscript to indicate their earliest API version. 
> The APIs of this module can be used only in the stage model.

```ts
import autoFillManager from '@ohos.app.ability.autoFillManager';
```

## Attributes

**System capability**: SystemCapability.Ability.AbilityRuntime.AbilityCore

| Name          | Value | Description                              |
| -------------- | --- | --------------------------------- |
| UNSPECIFIED      | 0   | Undefined type.                        |
| PASSWORD      | 1   | Password.                    |
| USER_NAME     | 2   | Username.                    |
| NEW_PASSWORD     | 3   | New password.                    |
