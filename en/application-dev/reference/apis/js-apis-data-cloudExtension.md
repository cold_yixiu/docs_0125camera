# @ohos.data.cloudExtension (Device-Cloud Sharing Extension)

The **cloudExtension** module provides APIs for third-party vendors to implement the device-cloud sharing service. You can use these APIs to share the device data to the server and implement device-cloud data sharing, including sharing and unsharing data, exiting a share, changing the privilege (operation permissions) on the shared data, querying participants by data identifier or invitation code, and confirming or changing a sharing invitation. Before you get started, it is helpful to understand the following concepts:

- **sharingResource**: an identifier of the string type generated for each data record shared by an application when device-cloud sync is performed. It uniquely identifies the data record being shared.
- **Participant**: all participants involved in a share, including the inviter and invitees.
- **invitationCode**: an invitation code generated by the share server for a share operation. It is generated after a share is initiated and attached to an invitation to be pushed to the devices of target invitees. The target invitees then confirm the invitation via this code.
- **CloudService**: device-cloud sync server, which implements data sync across devices with the same account for the same application.
- **ShareCenter**: device-cloud sharing server, which implements cross-account and cross-device data sharing for the same application.

> **NOTE**
>
> - The initial APIs of this module are supported since API version 11. Newly added APIs will be marked with a superscript to indicate their earliest API version.
>
> - The APIs provided by this module are system APIs.

## Modules to Import

```ts
import cloudExtension from '@ohos.data.cloudExtension';
```

## Result&lt;T&gt;

Represents the data sharing result.

**System capability**: SystemCapability.DistributedDataManager.CloudSync.Server

| Name         | Type                         | Mandatory | Description          |
| ----------- | --------------------------- | --- | ------------ |
| code        | number                      | Yes  | Error code.      |
| description | string                      | No  | Detailed description of the error code. The default value is **undefined**.      |
| value       | T                           | No  | Value returned. The specific type is specified by the **T** parameter. The default value is **undefined**.      |

## cloudExtension.createCloudServiceStub

createCloudServiceStub(instance: CloudService): Promise&lt;rpc.RemoteObject&gt;

Creates a [RemoteObject](js-apis-rpc.md#remoteobject) object based on a [CloudService](#cloudservice) instance. The system calls the [CloudService](#cloudservice) interface through this object. This API uses a promise to return the result.

**System capability**: SystemCapability.DistributedDataManager.CloudSync.Server

**Parameters**

| Name   | Type                           | Mandatory| Description                                                        |
| --------- | ------------------------------- | ---- | -------------------------------- |
| instance  | [CloudService](#cloudservice)   | Yes   | Instance of the [CloudService](#cloudservice) class.  |

**Return value**

| Type               | Description                     |
| -------------------             | ------------------------- |
| Promise&lt;[rpc.RemoteObject](js-apis-rpc.md#remoteobject)&gt; | Promise used to return the [RemoteObject](js-apis-rpc.md#remoteobject) object of [CloudService](#cloudservice) created.|

**Example**

```ts
import ServiceExtensionAbility from '@ohos.app.ability.ServiceExtensionAbility';
import Want from '@ohos.app.ability.Want';
import rpc from '@ohos.rpc';

export default class MyCloudService implements cloudExtension.CloudService {
  constructor() {}
  async connectShareCenter(userId: number, bundleName: string): Promise<rpc.RemoteObject> {
    // ...
  }
}

export default class MyServiceExtension extends ServiceExtensionAbility {
  onCreate(want: Want) {
    console.info(`onCreate: ${want}`);
  }
  onRequest(want: Want, startId: number) {
    console.info(`onRequest: ${want} ${startId}`);
  }
  onConnect(want: Want): rpc.RemoteObject | Promise<rpc.RemoteObject> {
    console.info(`onConnect: ${want}`);
    return cloudExtension.createCloudServiceStub(new MyCloudService());
  }
  onDisconnect(want: Want) {
    console.info(`onDisconnect: ${want}`);
  }
  onDestroy() {
    console.info(`onDestroy`);   
  }
}
```

## cloudExtension.createShareServiceStub

createShareServiceStub(instance: ShareCenter): Promise&lt;rpc.RemoteObject&gt;

Creates a [RemoteObject](js-apis-rpc.md#remoteobject) object based on a [ShareCenter](#sharecenter) instance. The system calls the [ShareCenter](#sharecenter) interface through this object. This API uses a promise to return the result.

**System capability**: SystemCapability.DistributedDataManager.CloudSync.Server

**Parameters**

| Name   | Type                           | Mandatory| Description                                                        |
| --------- | ------------------------------- | ---- | -------------------------------- |
| instance  | [ShareCenter](#sharecenter)   | Yes   | Instance of the [ShareCenter](#sharecenter) class.                  |

**Return value**

| Type               | Description                     |
| -------------------             | ------------------------- |
| Promise&lt;[rpc.RemoteObject](js-apis-rpc.md#remoteobject)&gt; | Promise used to return the [RemoteObject](js-apis-rpc.md#remoteobject) object of [ShareCenter](#sharecenter) created.|

**Example**

```ts
import rpc from '@ohos.rpc';

export default class MyShareCenter implements cloudExtension.ShareCenter {
  constructor() {}
  // ...
}

export default class MyCloudService implements cloudExtension.CloudService {
  constructor() {}
  async connectShareCenter(userId: number, bundleName: string): Promise<rpc.RemoteObject> {
    console.info(`connect share center, bundle: ${bundleName}`);
    return cloudExtension.createShareServiceStub(new MyShareCenter());
  }
}
```

## CloudService

Provides a class for interworking with the cloud sync service. You need to inherit this class and implement APIs of this class. The system calls these APIs to interact and use the cloud sync service.

### connectShareCenter

connectShareCenter(userId: number, bundleName: string): Promise&lt;rpc.RemoteObject&gt;

Obtains the [RemoteObject](js-apis-rpc.md#remoteobject) object of [ShareCenter](#sharecenter), which is created by [createShareServiceStub](#cloudextensioncreateshareservicestub). This API uses a promise to return the result.

**System capability**: SystemCapability.DistributedDataManager.CloudSync.Server

**Parameters**

| Name | Type                   | Mandatory| Description                                           |
| ------- | ----------------------- | ---- | ----------------------------------------------- |
| userId      | number  | Yes  | User ID.        |
| bundleName  | string  | Yes  | Bundle name of the application.  |

**Return value**

| Type               | Description                     |
| ------------------- | ------------------------- |
| Promise&lt;[rpc.RemoteObject](js-apis-rpc.md#remoteobject)&gt; | Promise used to return the [RemoteObject](js-apis-rpc.md#remoteobject) object of [ShareCenter](#sharecenter) obtained.|

**Example**

```ts
import rpc from '@ohos.rpc';

export default class MyShareCenter implements cloudExtension.ShareCenter {
  constructor() {}
  // ...
}

export default class MyCloudService implements cloudExtension.CloudService {
  constructor() {}
  async connectShareCenter(userId: number, bundleName: string): Promise<rpc.RemoteObject> {
    console.info(`connect share center, bundle: ${bundleName}`);
    return cloudExtension.createShareServiceStub(new MyShareCenter());
  }
}
```

## ShareCenter

Provides a class for interworking with the **sharedCenter** service. You need to inherit this class and implement APIs of this class. The system calls these APIs to initiate, cancel, or exit a device-cloud share.

### share

share(userId: number, bundleName: string, sharingResource: string, participants: Array&lt;cloudData.sharing.Participant&gt;): Promise&lt;Result&lt;Array&lt;Result&lt;cloudData.sharing.Participant&gt;&gt;&gt;&gt;

Shares data. This API uses a promise to return the result. The application that initiates the share, shared resource ID, participants of the share need to be specified.

**System capability**: SystemCapability.DistributedDataManager.CloudSync.Server

**Parameters**

| Name | Type                   | Mandatory| Description                                           |
| ------- | ----------------------- | ---- | ----------------------------------------------- |
| userId          | number  | Yes  | User ID. |
| bundleName      | string  | Yes  | Bundle name of the application.   |
| sharingResource | string  | Yes  | Shared resource ID.  |
| participants    | Array&lt;[cloudData.sharing.Participant](#js-apis-data-cloudData.md#participant11)&gt;  | Yes  | Participants of the share.  |

**Return value**

| Type               | Description                     |
| ------------------- | ------------------------- |
| Promise&lt;[Result](#resultt)&lt;Array&lt;[Result](#resultt)&lt;[cloudData.sharing.Participant](#js-apis-data-cloudData.md#participant11)&gt;&gt;&gt;&gt; | Promise used to return the result.|

**Example**

```ts
import cloudData from '@ohos.data.cloudData';

type Participant = cloudData.sharing.Participant;

export default class MyShareCenter implements cloudExtension.ShareCenter {
  constructor() {}
  async share(userId: number, bundleName: string, sharingResource: string, participants: Array<Participant>):
    Promise<cloudExtension.Result<Array<cloudExtension.Result<Participant>>>> {
    console.info(`share, bundle: ${bundleName}`);
    // Connect to ShareCenter and obtain the return value.
    // ...
    // Return the result obtained from ShareCenter.
    let result: Array<cloudExtension.Result<Participant>> = [];
    participants.forEach((item => {
      result.push({
        code: cloudData.sharing.sharingCode.SUCCESS,
        description: 'share succeeded'    
      })
    }))
    return {
      code: cloudData.sharing.sharingCode.SUCCESS,
      description: 'share succeeded',
      value: result
    }
  }
  // ...
}
```

### unshare

unshare(userId: number, bundleName: string, sharingResource: string, participants: Array&lt;cloudData.sharing.Participant&gt;): Promise&lt;Result&lt;Array&lt;Result&lt;cloudData.sharing.Participant&gt;&gt;&gt;&gt;

Unshares data. This API uses a promise to return the result. The application, shared resource ID, and participants for the data to unshare need to be specified.

**System capability**: SystemCapability.DistributedDataManager.CloudSync.Server

**Parameters**

| Name | Type                   | Mandatory| Description                                           |
| ------- | ----------------------- | ---- | ----------------------------------------------- |
| userId          | number  | Yes  | User ID. |
| bundleName      | string  | Yes  | Bundle name of the application.   |
| sharingResource | string  | Yes  | Shared resource ID.  |
| participants    | Array&lt;[cloudData.sharing.Participant](#js-apis-data-cloudData.md#participant11)&gt;  | Yes  | Participants of the share.  |

**Return value**

| Type               | Description                     |
| ------------------- | ------------------------- |
| Promise&lt;[Result](#resultt)&lt;Array&lt;[Result](#resultt)&lt;[cloudData.sharing.Participant](#js-apis-data-cloudData.md#participant11)&gt;&gt;&gt;&gt; | Promise used to return the result.|

**Example**

```ts
import cloudData from '@ohos.data.cloudData';

type Participant = cloudData.sharing.Participant;

export default class MyShareCenter implements cloudExtension.ShareCenter {
  constructor() {}
  async unshare(userId: number, bundleName: string, sharingResource: string, participants: Array<Participant>):
    Promise<cloudExtension.Result<Array<cloudExtension.Result<Participant>>>> {
    console.info(`unshare, bundle: ${bundleName}`);
    //Connect to ShareCenter and obtain the return value of the unshare operation.
    // ...
    // Return the result obtained from ShareCenter.
    let result: Array<cloudExtension.Result<Participant>> = [];
    participants.forEach((item => {
      result.push({
        code: cloudData.sharing.sharingCode.SUCCESS,
        description: 'unshare succeeded'    
      })
    }))
    return {
      code: cloudData.sharing.sharingCode.SUCCESS,
      description: 'unshare succeeded',
      value: result
    }
  }
  // ...
}
```

### exit

exit(userId: number, bundleName: string, sharingResource: string): Promise&lt;Result&lt;void&gt;&gt;

Exits a device-cloud share. This API uses a promise to return the result. The application and shared resource ID need to be specified.

**System capability**: SystemCapability.DistributedDataManager.CloudSync.Server

**Parameters**

| Name | Type                   | Mandatory| Description                                           |
| ------- | ----------------------- | ---- | ----------------------------------------------- |
| userId          | number  | Yes  | User ID. |
| bundleName      | string  | Yes  | Bundle name of the application.   |
| sharingResource | string  | Yes  | Shared resource ID.  |

**Return value**

| Type               | Description                     |
| ------------------- | ------------------------- |
| Promise&lt;[Result](#resultt)&lt;void&gt;&gt; | Promise used to return the result.|

**Example**

```ts
import cloudData from '@ohos.data.cloudData';

export default class MyShareCenter implements cloudExtension.ShareCenter {
  constructor() {}
  async exit(userId: number, bundleName: string, sharingResource: string):
    Promise<cloudExtension.Result<void>> {
    console.info(`exit share, bundle: ${bundleName}`);
    // Connect to ShareCenter and obtain the return value of the exit operation.
    // ...
    // Return the result obtained from ShareCenter.
    return {
      code: cloudData.sharing.sharingCode.SUCCESS,
      description: 'exit share succeeded'
    }
  }
  // ...
}
```

### changePrivilege

changePrivilege(userId: number, bundleName: string, sharingResource: string, participants: Array&lt;cloudData.sharing.Participant&gt;): Promise&lt;Result&lt;Array&lt;Result&lt;cloudData.sharing.Participant&gt;&gt;&gt;&gt;

Changes the privilege (operation permissions) on the shared data. This API uses a promise to return the result. The application, shared resource ID, and the participants with new privilege need to be specified.

**System capability**: SystemCapability.DistributedDataManager.CloudSync.Server

**Parameters**

| Name | Type                   | Mandatory| Description                                           |
| ------- | ----------------------- | ---- | ----------------------------------------------- |
| userId          | number  | Yes  | User ID. |
| bundleName      | string  | Yes  | Bundle name of the application.   |
| sharingResource | string  | Yes  | Shared resource ID.  |
| participants    | Array&lt;[cloudData.sharing.Participant](#js-apis-data-cloudData.md#participant11)&gt;  | Yes  | Participants with new privilege.  |

**Return value**

| Type               | Description                     |
| ------------------- | ------------------------- |
| Promise&lt;[Result](#resultt)&lt;Array&lt;[Result](#resultt)&lt;[cloudData.sharing.Participant](#js-apis-data-cloudData.md#participant11)&gt;&gt;&gt;&gt; | Promise used to return the result.|

**Example**

```ts
import cloudData from '@ohos.data.cloudData';

type Participant = cloudData.sharing.Participant;

export default class MyShareCenter implements cloudExtension.ShareCenter {
  constructor() {}
  async changePrivilege(userId: number, bundleName: string, sharingResource: string, participants: Array<Participant>):
    Promise<cloudExtension.Result<Array<cloudExtension.Result<Participant>>>> {
    console.info(`change privilege, bundle: ${bundleName}`);
    // Connect to ShareCenter and obtain the return value of the privilege change operation.
    // ...
    // Return the result obtained from ShareCenter.
    let result: Array<cloudExtension.Result<Participant>> = [];
    participants.forEach((item => {
      result.push({
        code: cloudData.sharing.sharingCode.SUCCESS,
        description: 'change privilege succeeded'    
      })
    }))
    return {
      code: cloudData.sharing.sharingCode.SUCCESS,
      description: 'change privilege succeeded',
      value: result
    }
  }
  // ...
}
```

### queryParticipants

queryParticipants(userId: number, bundleName: string, sharingResource: string): Promise&lt;Result&lt;Array&lt;cloudData.sharing.Participant&gt;&gt;&gt;

Queries the participants of a share. This API uses a promise to return the result. The application and shared resource ID need to be specified.

**System capability**: SystemCapability.DistributedDataManager.CloudSync.Server

**Parameters**

| Name | Type                   | Mandatory| Description                                           |
| ------- | ----------------------- | ---- | ----------------------------------------------- |
| userId          | number  | Yes  | User ID. |
| bundleName      | string  | Yes  | Bundle name of the application.   |
| sharingResource | string  | Yes  | Shared resource ID.  |

**Return value**

| Type               | Description                     |
| ------------------- | ------------------------- |
| Promise&lt;[Result](#resultt)&lt;Array&lt;[cloudData.sharing.Participant](#js-apis-data-cloudData.md#participant11)&gt;&gt;&gt; | Promise used to return the participants obtained.|

**Example**

```ts
import cloudData from '@ohos.data.cloudData';

type Participant = cloudData.sharing.Participant;

export default class MyShareCenter implements cloudExtension.ShareCenter {
  constructor() {}
  async queryParticipants(userId: number, bundleName: string, sharingResource: string):
    Promise<cloudExtension.Result<Array<Participant>>> {
    console.info(`query participants, bundle: ${bundleName}`);
    // Connect to ShareCenter and obtain the return value of the query operation.
    // ...
    // Return the result obtained from ShareCenter.
    let privilege = {
      writable: false,
      readable: true,
      creatable: false,
      deletable: false,
      shareable: false
    }
    let participants = new Array();
    participants.push({
      identity: '000000000',
      role: cloudData.sharing.Role.ROLE_INVITEE,
      state: cloudData.sharing.State.STATE_ACCEPTED,
      privilege: privilege,
      attachInfo: ''
    })
    participants.push({
      identity: '111111111',
      role: cloudData.sharing.Role.ROLE_INVITEE,
      state: cloudData.sharing.State.STATE_ACCEPTED,
      privilege: privilege,
      attachInfo: ''
    })
    return {
      code: cloudData.sharing.sharingCode.SUCCESS,
      description: 'query participants succeeded',
      value: participants
    }
  }
  // ...
}
```

### queryParticipantsByInvitation

queryParticipantsByInvitation(userId: number, bundleName: string, invitationCode: string): Promise&lt;Result&lt;Array&lt;cloudData.sharing.Participant&gt;&gt;&gt;

Queries the participants of a share based on the invitation code. This API uses a promise to return the result. The application and the invitation code of the shared data need to be specified.

**System capability**: SystemCapability.DistributedDataManager.CloudSync.Server

**Parameters**

| Name | Type                   | Mandatory| Description                                           |
| ------- | ----------------------- | ---- | ----------------------------------------------- |
| userId          | number  | Yes  | User ID. |
| bundleName      | string  | Yes  | Bundle name of the application.   |
| invitationCode  | string  | Yes  | Invitation code for the share.  |

**Return value**

| Type               | Description                     |
| ------------------- | ------------------------- |
| Promise&lt;[Result](#resultt)&lt;Array&lt;[cloudData.sharing.Participant](#js-apis-data-cloudData.md#participant11)&gt;&gt;&gt; | Promise used to return the participants obtained.|

**Example**

```ts
import cloudData from '@ohos.data.cloudData';

type Participant = cloudData.sharing.Participant;

export default class MyShareCenter implements cloudExtension.ShareCenter {
  constructor() {}
  async queryParticipantsByInvitation(userId: number, bundleName: string, invitationCode: string):
    Promise<cloudExtension.Result<Array<Participant>>> {
    console.info(`query participants by invitation, bundle: ${bundleName}`);
    // Connect to ShareCenter and obtain the return value of the query operation.
    // ...
    // Return the result obtained from ShareCenter.
    let privilege = {
      writable: false,
      readable: true,
      creatable: false,
      deletable: false,
      shareable: false
    }
    let participants = new Array();
    participants.push({
      identity: '000000000',
      role: cloudData.sharing.Role.ROLE_INVITEE,
      state: cloudData.sharing.State.STATE_ACCEPTED,
      privilege: privilege,
      attachInfo: ''
    })
    participants.push({
      identity: '111111111',
      role: cloudData.sharing.Role.ROLE_INVITEE,
      state: cloudData.sharing.State.STATE_ACCEPTED,
      privilege: privilege,
      attachInfo: ''
    })
    return {
      code: cloudData.sharing.sharingCode.SUCCESS,
      description: 'query participants by invitation succeeded',
      value: participants
    }
  }
  // ...
}
```

### confirmInvitation

confirmInvitation(userId: number, bundleName: string, invitationCode: string, state: cloudData.sharing.State): Promise&lt;Result&lt;string&gt;&gt;

Confirms the invitation for a share. This API uses a promise to return the result. The application, invitation code for the share, and the confirmation state need to be specified.

**System capability**: SystemCapability.DistributedDataManager.CloudSync.Server

**Parameters**

| Name | Type                   | Mandatory| Description                                           |
| ------- | ----------------------- | ---- | ----------------------------------------------- |
| userId          | number  | Yes  | User ID. |
| bundleName      | string  | Yes  | Bundle name of the application.   |
| invitationCode  | string  | Yes  | Invitation code for the share.  |
| state           | [cloudData.sharing.State](#js-apis-data-cloudData.md#state11)  | Yes  | Confirmation state of the invitation.  |

**Return value**

| Type               | Description                     |
| ------------------- | ------------------------- |
| Promise&lt;[Result](#resultt)&lt;string&gt;&gt; | Promise used to return the shared resource ID.|

**Example**

```ts
import cloudData from '@ohos.data.cloudData';

export default class MyShareCenter implements cloudExtension.ShareCenter {
  constructor() {}
  async confirmInvitation(userId: number, bundleName: string, invitationCode: string, state: cloudData.sharing.State):
    Promise<cloudExtension.Result<string>> {
    console.info(`confirm invitation, bundle: ${bundleName}`);
    // Connect to ShareCenter and obtain the return value of the invitation confirmation operation.
    // ...
    // Return the result obtained from ShareCenter.
    return {
      code: cloudData.sharing.sharingCode.SUCCESS,
      description: 'confirm invitation succeeded',
      value: 'sharing_resource_test'
    }
  }
  // ...
}
```

### changeConfirmation

changeConfirmation(userId: number, bundleName: string, sharingResource: string, state: cloudData.sharing.State): Promise&lt;Result&lt;void&gt;&gt;

Changes the confirmation state of a share invitation. This API uses a promise to return the result. The application, shared resource ID, and the new conformation state need to be specified.

**System capability**: SystemCapability.DistributedDataManager.CloudSync.Server

**Parameters**

| Name | Type                   | Mandatory| Description                                           |
| ------- | ----------------------- | ---- | ----------------------------------------------- |
| userId          | number  | Yes  | User ID. |
| bundleName      | string  | Yes  | Bundle name of the application.   |
| sharingResource | string  | Yes  | Shared resource ID.  |
| state           | [cloudData.sharing.State](#js-apis-data-cloudData.md#state11)  | Yes  | New confirmation state.  |

**Return value**

| Type               | Description                     |
| ------------------- | ------------------------- |
| Promise&lt;[Result](#resultt)&lt;void&gt;&gt; | Promise used to return the result.|

**Example**

```ts
import cloudData from '@ohos.data.cloudData';

export default class MyShareCenter implements cloudExtension.ShareCenter {
  constructor() {}
  async changeConfirmation(userId: number, bundleName: string, sharingResource: string, state: cloudData.sharing.State):
    Promise<cloudExtension.Result<void>> {
    console.info(`change confirm, bundle: ${bundleName}`);
    // Connect to ShareCenter and obtain the return value of the state change operation.
    // ...
    // Return the result obtained from ShareCenter.
    return {
      code: cloudData.sharing.sharingCode.SUCCESS,
      description: 'change confirm succeeded'
    }
  }
  // ...
}
```

 <!--no_check--> 