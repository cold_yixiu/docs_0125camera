# Camera_VideoProfile


## Overview

The **Camera_VideoProfile** struct defines the video profile.

**Since**: 11

**Related module**: [OH_Camera](_o_h___camera.md)


## Summary


### Member Variables

| Name| Description|
| -------- | -------- |
| [format](#format) | Camera format.|
| [size](#size) | Image size.|
| [range](#range) | Frame rate, in frames per second (FPS).|


## Member Variable Description


### format

```
Camera_Format Camera_VideoProfile::format
```

**Description**

Camera format.


### range

```
Camera_FrameRateRange Camera_VideoProfile::range
```

**Description**

Frame rate, in FPS.


### size

```
Camera_Size Camera_VideoProfile::size
```

**Description**

Image size.
