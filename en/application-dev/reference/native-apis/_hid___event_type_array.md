# Hid_EventTypeArray


## Overview

Defines an array of event types.

**Since**: 11

**Related module**: [HidDdk](_hid_ddk.md)


## Summary


### Member Variables

| Name| Description| 
| -------- | -------- |
| [hidEventType](_hid_ddk.md#hideventtype) | [Hid_EventType](_hid_ddk.md#hid_eventtype) \* | 
| [length](_hid_ddk.md#length-15) | uint16_t | 
