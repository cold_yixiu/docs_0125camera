# drawing_text_declaration.h


## Overview

The **drawing_text_declaration.h** file declares the structs related to text in 2D drawing.

**File to include**: &lt;native_drawing/drawing_text_declaration.h&gt;

**Library**: libnative_drawing.so

**Since**: 8

**Related module**: [Drawing](_drawing.md)


## Summary


### Types

| Name| Description|
| -------- | -------- |
| [OH_Drawing_FontCollection](_drawing.md#oh_drawing_fontcollection) | Defines an **OH_Drawing_FontCollection**, which is used to load fonts.|
| [OH_Drawing_Typography](_drawing.md#oh_drawing_typography) | Defines an **OH_Drawing_Typography**, which is used to manage the typography layout and display.|
| [OH_Drawing_TextStyle](_drawing.md#oh_drawing_textstyle) | Defines an **OH_Drawing_TextStyle**, which is used to manage text colors and decorations.|
| [OH_Drawing_TypographyStyle](_drawing.md#oh_drawing_typographystyle) | Defines an **OH_Drawing_TypographyStyle**, which is used to manage the typography style, such as the text direction.|
| [OH_Drawing_TypographyCreate](_drawing.md#oh_drawing_typographycreate) | Creates an [OH_Drawing_Typography](_drawing.md#oh_drawing_typography).|
| [OH_Drawing_TextBox](_drawing.md#oh_drawing_textbox) | Defines an **OH_Drawing_TextBox**, which is used to receive the rectangle size, direction, and quantity of text boxes.|
| [OH_Drawing_PositionAndAffinity](_drawing.md#oh_drawing_positionandaffinity) | Defines an **OH_Drawing_PositionAndAffinity**, which is used to receive the position and affinity of the font.|
| [OH_Drawing_Range](_drawing.md#oh_drawing_range) | Defines an **OH_Drawing_Range**, which is used to receive the start position and end position of the font.|
