# Camera_FrameShutterInfo


## Overview

The **Camera_FrameShutterInfo** struct defines the frame shutter callback.

**Since**: 11

**Related module**: [OH_Camera](_o_h___camera.md)


## Summary


### Member Variables

| Name| Description|
| -------- | -------- |
| [captureId](#captureid) | Capture ID.|
| [timestamp](#timestamp) | Timestamp of the frame captured.|


## Member Variable Description


### captureId

```
int32_t Camera_FrameShutterInfo::captureId
```

**Description**

Capture ID.


### timestamp

```
uint64_t Camera_FrameShutterInfo::timestamp
```

**Description**

Timestamp of the frame captured.
