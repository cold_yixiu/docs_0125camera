# ImagePacker_Opts_


## Overview

The **ImagePacker_Opts_** struct defines the image encoding options.

**System capability**: SystemCapability.Multimedia.Image

**Since**: 11

**Related module**: [Image](image.md)


## Summary


### Member Variables

| Name| Description| 
| -------- | -------- |
| [format](#format) | Encoding format.| 
| [quality](#quality) | Encoding quality.| 


## Member Variable Description


### format

```
const char* ImagePacker_Opts_::format
```

**Description**

Encoding format.


### quality

```
int ImagePacker_Opts_::quality
```

**Description**

Encoding quality.
