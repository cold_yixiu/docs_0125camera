# OhosImageReceiverInfo


## Overview

The **OhosImageReceiverInfo** struct defines the information about an image receiver.

**System capability**: SystemCapability.Multimedia.Image

**Since**: 10

**Related module**: [Image](image.md)


## Summary


### Member Variables

| Name| Description| 
| -------- | -------- |
| width | Default width of the image received by the consumer, in pixels.| 
| height | Default height of the image received by the consumer, in pixels.| 
| format | Image format **OHOS_IMAGE_FORMAT_JPEG** created by using the receiver.| 
| capicity | Maximum number of images that can be cached.| 
