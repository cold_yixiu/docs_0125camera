# Camera_Device


## Overview

The **Camera_Device** struct defines the camera device.

**Since**: 11

**Related module**: [OH_Camera](_o_h___camera.md)


## Summary


### Member Variables

| Name| Description|
| -------- | -------- |
| \*[cameraId](#cameraid) | Camera ID.|
| [cameraPosition](#cameraposition) | Camera position.|
| [cameraType](#cameratype) | Camera type.|
| [connectionType](#connectiontype) | Camera connection type.|


## Member Variable Description


### cameraId

```
char* Camera_Device::cameraId
```

**Description**

Camera ID.


### cameraPosition

```
Camera_Position Camera_Device::cameraPosition
```

**Description**

Camera position.


### cameraType

```
Camera_Type Camera_Device::cameraType
```

**Description**

Camera type.


### connectionType

```
Camera_Connection Camera_Device::connectionType
```

**Description**

Camera connection type.
