# Hid_EventProperties


## Overview

Defines the event properties of a device.

**Since**: 11

**Related module**: [HidDdk](_hid_ddk.md)


## Summary


### Member Variables

| Name| Description| 
| -------- | -------- |
| [hidEventTypes](_hid_ddk.md#hideventtypes) | struct [Hid_EventTypeArray](_hid___event_type_array.md) | 
| [hidKeys](_hid_ddk.md#hidkeys) | struct [Hid_KeyCodeArray](_hid___key_code_array.md) | 
| [hidAbs](_hid_ddk.md#hidabs) | struct [Hid_AbsAxesArray](_hid___abs_axes_array.md) | 
| [hidRelBits](_hid_ddk.md#hidrelbits) | struct [Hid_RelAxesArray](_hid___rel_axes_array.md) | 
| [hidMiscellaneous](_hid_ddk.md#hidmiscellaneous) | struct [Hid_MscEventArray](_hid___msc_event_array.md) | 
| [hidAbsMax](_hid_ddk.md#hidabsmax) [64] | int32_t | 
| [hidAbsMin](_hid_ddk.md#hidabsmin) [64] | int32_t | 
| [hidAbsFuzz](_hid_ddk.md#hidabsfuzz) [64] | int32_t | 
| [hidAbsFlat](_hid_ddk.md#hidabsflat) [64] | int32_t | 
