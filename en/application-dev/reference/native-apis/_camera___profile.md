# Camera_Profile


## Overview

The **Camera_Profile** struct defines the profile of the camera stream.

**Since**: 11

**Related module**: [OH_Camera](_o_h___camera.md)


## Summary


### Member Variables

| Name| Description| 
| -------- | -------- |
| [format](#format) | Camera format.| 
| [size](#size) | Image size.| 


## Member Variable Description


### format

```
Camera_Format Camera_Profile::format
```

**Description**

Camera format.


### size

```
Camera_Size Camera_Profile::size
```

**Description**

Image size.
