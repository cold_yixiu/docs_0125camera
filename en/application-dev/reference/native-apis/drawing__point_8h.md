# drawing_point.h


## Overview

The **drawing_point.h** file declares the functions related to the coordinate point in the drawing module.

**File to include**: &lt;native_drawing/drawing_point.h&gt;

**Library**: libnative_drawing.so

**Since**: 11

**Related module**: [Drawing](_drawing.md)


## Summary


### Functions

| Name| Description|
| -------- | -------- |
| [OH_Drawing_Point](_drawing.md#oh_drawing_point) \* [OH_Drawing_PointCreate](_drawing.md#oh_drawing_pointcreate) (float x, float y) | Creates an **OH_Drawing_Point** object.|
| void [OH_Drawing_PointDestroy](_drawing.md#oh_drawing_pointdestroy) ([OH_Drawing_Point](_drawing.md#oh_drawing_point) \*) | Destroys an **OH_Drawing_Point** object and reclaims the memory occupied by the object.|
