# Hid_AbsAxesArray


## Overview

Defines an array of absolute coordinates.

**Since**: 11

**Related module**: [HidDdk](_hid_ddk.md)


## Summary


### Member Variables

| Name| Description|
| -------- | -------- |
| [hidAbsAxes](_hid_ddk.md#hidabsaxes) | [Hid_AbsAxes](_hid_ddk.md#hid_absaxes) \* |
| [length](_hid_ddk.md#length-35) | uint16_t |
