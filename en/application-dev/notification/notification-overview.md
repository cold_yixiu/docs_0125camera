# Introduction to Notification Kit

With Notification Kit, you can swiftly publish local notifications from your application, which can be displayed in various system windows, such as notification panel, banners, lock screen, and Always On Display (AOD) – a convenient way to interact with the user outside of the application screen.

## When to Use

Typically, you can use local notifications to:

- Display received instant messages and SMS messages.
- Display ongoing events, such as the download progress.

## Working Principles

The notification service process involves the notification subsystem, notification sender, and notification subscriber. A notification is generated by the notification sender and sent to the notification subsystem through [inter-process communication (IPC)](../connectivity/ipc-rpc-overview.md). The notification subsystem then distributes the notification to the notification subscriber.

* Notification sender: It can be a third-party application or a system application. Pay special attention to this role.

* Notification subscriber: It can only be a system application, for example, the notification center. By default, the notification center subscribes to notifications sent by all applications on the current device to the current user. You do not need to pay attention to this role.

**Figure 1** Notification service process 

![en-us_image_0000001466582017](figures/en-us_image_0000001466582017.png)


## Capability Scope

- Various types of notifications
- A range of notification slots for publishing notifications, which can be easily managed, for example, adding or deleting a notification slot and checking the enabled status
- Easy management of published notifications, for example, canceling published notifications and querying all published notifications
- Requesting notification authorization and checking notification authorization status
- Notification badge management
- Management of wants for notification touches, such as redirecting the user to a UIAbility or publishing a custom common event.


## Relationship with Related Kits

- Notifications created by Notification Kit are displayed in the notification panel in real time. To publish scheduled notifications when your application is in the background or is not running, you can use BackGroundTask Kit. For example, you can use it to publish a flash sale reminder for your shopping application. Currently, the notification reminder feature is available for countdown, calendar, and alarm events.
- You can use Push Kit to push remote notifications to the local device.
- You can use Ability Kit to set wants for notification touches.


## Constraints

- There is a limit on the number of notifications per application in the system windows. The current limit is 24.
- A basic notification cannot exceed 200 KB (due to cross-process serialization).
- The total number of notifications published by all system applications cannot exceed 10 per second.

